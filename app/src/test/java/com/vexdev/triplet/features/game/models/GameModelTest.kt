package com.vexdev.triplet.features.game.models

import com.vexdev.triplet.features.game.models.GameModel.Companion.NA
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

class GameModelTest {

    @Test
    fun initialBoardStateIsEmpty() {
        val model = GameModel()

        Assert.assertThat(model.board, CoreMatchers.equalTo(mutableListOf(
                NA, NA, NA,
                NA, NA, NA,
                NA, NA, NA
        )))
    }

}
