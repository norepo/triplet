package com.vexdev.triplet.features.game.usecases

import com.vexdev.triplet.R
import com.vexdev.triplet.core.mvvm.events.ClickEvent
import com.vexdev.triplet.core.mvvm.events.UiEvent
import com.vexdev.triplet.features.game.models.GameModel
import com.vexdev.triplet.features.game.models.GameModel.Companion.CROSS
import com.vexdev.triplet.features.game.models.GameModel.Companion.NA
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test

class OnGameInputUpdateGameModelTest {

    private lateinit var useCase: OnGameInputUpdateGameModel
    private lateinit var uiEvents: PublishSubject<UiEvent>

    @Before
    fun setUp() {
        useCase = OnGameInputUpdateGameModel()
        uiEvents = PublishSubject.create()
    }

    @Test
    fun updatesClickedPosition() {
        val emptyModel = GameModel(playerPiece = CROSS)
        val observer = useCase.getUpdatedBoard(uiEvents, emptyModel).test()

        uiEvents.onNext(ClickEvent(R.id.pos0))

        observer.assertValue(mutableListOf(
                CROSS, NA, NA,
                NA, NA, NA,
                NA, NA, NA
        ))
    }
}