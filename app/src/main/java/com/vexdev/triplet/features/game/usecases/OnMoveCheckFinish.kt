package com.vexdev.triplet.features.game.usecases

import com.vexdev.triplet.features.game.models.GameModel

/**
 * Verifies if the match is concluded.
 */
class OnMoveCheckFinish {

    /**
     * Returns 0 for no winners, [GameModel.CROSS] or [GameModel.CIRCLE] for a winner.
     */
    fun isMatchOver(gameModel: GameModel): Int {
        for (x in winnerPatterns) {
            var crossSum = 0
            var circleSum = 0
            (0..8).filter { x[it] == 1 }
                    .forEach {
                        when (gameModel.board[it]) {
                            GameModel.CROSS -> crossSum++
                            GameModel.CIRCLE -> circleSum++
                        }
                    }
            if (crossSum == 3) {
                return GameModel.STATUS_WIN_CROSS
            }
            if (circleSum == 3) {
                return GameModel.STATUS_WIN_CIRCLE
            }
        }
        if (gameModel.turn > 8) {
            return GameModel.STATUS_DRAW
        }
        return GameModel.STATUS_PENDING
    }

    companion object {
        val winnerPatterns =
                arrayOf(
                        arrayOf(1, 1, 1, 0, 0, 0, 0, 0, 0),
                        arrayOf(0, 0, 0, 1, 1, 1, 0, 0, 0),
                        arrayOf(0, 0, 0, 0, 0, 0, 1, 1, 1),
                        arrayOf(1, 0, 0, 1, 0, 0, 1, 0, 0),
                        arrayOf(0, 1, 0, 0, 1, 0, 0, 1, 0),
                        arrayOf(0, 0, 1, 0, 0, 1, 0, 0, 1),
                        arrayOf(1, 0, 0, 0, 1, 0, 0, 0, 1),
                        arrayOf(0, 0, 1, 0, 1, 0, 1, 0, 0)
                )
    }

}