package com.vexdev.triplet.features.game.viewmodels

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.view.View.*
import com.vexdev.triplet.R
import com.vexdev.triplet.core.mvvm.ViewModel
import com.vexdev.triplet.features.game.models.GameModel
import com.vexdev.triplet.features.game.models.GameModel.Companion.CIRCLE
import com.vexdev.triplet.features.game.models.GameModel.Companion.CROSS
import com.vexdev.triplet.features.game.models.GameModel.Companion.STATUS_DRAW
import com.vexdev.triplet.features.game.models.GameModel.Companion.STATUS_PENDING
import com.vexdev.triplet.features.game.models.GameModel.Companion.STATUS_WIN_CIRCLE
import com.vexdev.triplet.features.game.models.GameModel.Companion.STATUS_WIN_CROSS
import com.vexdev.triplet.features.game.models.GameModel.GameType.MULTI_PLAYER

class GameViewModel(private val context: Context) : ViewModel<GameModel>(GameModel()) {

    val playerDrawable get() = getDrawable(model.playerPiece)
    val playingAsVisibility get() = if (model.gameType == MULTI_PLAYER) INVISIBLE else VISIBLE
    val winningBoxVisibility get() = if (model.gameStatus == STATUS_PENDING) GONE else VISIBLE
    val winningText: String
        get() {
            if (model.gameStatus == STATUS_DRAW) {
                return context.getString(R.string.draw)
            }
            if (model.gameType == MULTI_PLAYER) {
                return context.getString(R.string.game_over)
            }
            if (model.gameStatus == STATUS_WIN_CROSS && model.playerPiece == CROSS) {
                return context.getString(R.string.you_won)
            }
            if (model.gameStatus == STATUS_WIN_CIRCLE && model.playerPiece == CIRCLE) {
                return context.getString(R.string.you_won)
            }
            return context.getString(R.string.game_over)
        }

    val position0Drawable get() = getDrawable(model.board[0])
    val position1Drawable get() = getDrawable(model.board[1])
    val position2Drawable get() = getDrawable(model.board[2])
    val position3Drawable get() = getDrawable(model.board[3])
    val position4Drawable get() = getDrawable(model.board[4])
    val position5Drawable get() = getDrawable(model.board[5])
    val position6Drawable get() = getDrawable(model.board[6])
    val position7Drawable get() = getDrawable(model.board[7])
    val position8Drawable get() = getDrawable(model.board[8])

    private fun getDrawable(piece: Int): Drawable? = when (piece) {
        GameModel.CIRCLE -> ContextCompat.getDrawable(context, R.drawable.ic_circle)
        GameModel.CROSS -> ContextCompat.getDrawable(context, R.drawable.ic_cross)
        else -> null
    }

}