package com.vexdev.triplet.features.intro.presenters

import com.vexdev.triplet.R
import com.vexdev.triplet.core.TripletApplication
import com.vexdev.triplet.core.extensions.uievent.clickEvents
import com.vexdev.triplet.core.mvvm.ViewModel
import com.vexdev.triplet.core.mvvm.events.ClickEvent
import com.vexdev.triplet.core.mvvm.events.Irrelevant
import com.vexdev.triplet.core.mvvm.events.UiEvent
import com.vexdev.triplet.core.navigation.Presenter
import com.vexdev.triplet.core.navigation.Router
import com.vexdev.triplet.databinding.ScreenGameTypeBinding
import io.reactivex.Observable

/**
 * Allows to chose between multiplayer and singleplayer game.
 */
class GameTypePresenter : Presenter<Any, ScreenGameTypeBinding> {
    private val router: Router = TripletApplication.serviceLocator.get()!!

    override fun layout(): Int = R.layout.screen_game_type
    override fun viewModelId(): Int = 0
    override val viewModel: ViewModel<Any>? = null

    override val eventGenerator: (ScreenGameTypeBinding) -> Observable<UiEvent> get() = {
        Observable.merge(listOf(
                it.easy.clickEvents(),
                it.multi.clickEvents()
        ))
    }

    override fun setupSubscriptions(
            uiEvents: Observable<UiEvent>,
            stopEvent: Observable<Irrelevant>) {
        uiEvents
                .ofType(ClickEvent::class.java)
                .takeUntil(stopEvent)
                .firstElement()
                .subscribe {
                    when(it.viewId) {
                        R.id.easy -> router.easyGame()
                        R.id.multi -> router.multiGame()
                    }
                }
    }

}