package com.vexdev.triplet.features.game.presenters

import com.vexdev.triplet.BR
import com.vexdev.triplet.R
import com.vexdev.triplet.core.TripletApplication
import com.vexdev.triplet.core.extensions.uievent.clickEvents
import com.vexdev.triplet.core.mvvm.ViewModel
import com.vexdev.triplet.core.mvvm.events.ClickEvent
import com.vexdev.triplet.core.mvvm.events.Irrelevant
import com.vexdev.triplet.core.mvvm.events.UiEvent
import com.vexdev.triplet.core.navigation.Presenter
import com.vexdev.triplet.core.navigation.Router
import com.vexdev.triplet.databinding.ScreenGameBinding
import com.vexdev.triplet.features.UseCasesFactory
import com.vexdev.triplet.features.ViewModelFactory
import com.vexdev.triplet.features.game.models.GameModel
import io.reactivex.Observable

class GamePresenter(private val gameType: GameModel.GameType) : Presenter<GameModel, ScreenGameBinding> {

    private val router: Router = TripletApplication.serviceLocator.get()!!
    private val viewModelFactory: ViewModelFactory = TripletApplication.serviceLocator.get()!!
    private val useCasesFactory: UseCasesFactory = TripletApplication.serviceLocator.get()!!
    private val onGameInputUpdateModel = useCasesFactory.newOnGameInputUpdateGameModel()
    private val onMoveCheckFinish = useCasesFactory.newOnMoveCheckFinish()
    private val autoMove = useCasesFactory.newAutomatedMove()

    override fun layout(): Int = R.layout.screen_game
    override fun viewModelId(): Int = BR.model
    override val viewModel: ViewModel<GameModel> = viewModelFactory
            .newGameViewModel()
            .apply { model = model.copy(gameType = gameType) }

    /**
     * todo: Perhaps split this into a separate class? (The binding is not easily testable)
     * Besides, in a production app we will most probably also need other android-relevant
     * configurations in which case it would be good to extract all this "android binding" stuff in
     * a separate class.
     */
    override val eventGenerator: (ScreenGameBinding) -> Observable<UiEvent> get() = {
        Observable.merge(listOf(
                it.replay.clickEvents(),
                it.pos0.clickEvents(),
                it.pos1.clickEvents(),
                it.pos2.clickEvents(),
                it.pos3.clickEvents(),
                it.pos4.clickEvents(),
                it.pos5.clickEvents(),
                it.pos6.clickEvents(),
                it.pos7.clickEvents(),
                it.pos8.clickEvents()
        ))
    }

    override fun setupSubscriptions(
            uiEvents: Observable<UiEvent>,
            stopEvent: Observable<Irrelevant>) {
        uiEvents
                .ofType(ClickEvent::class.java)
                .takeUntil(stopEvent)
                .map { onGameInputUpdateModel.getUpdatedBoard(it, viewModel.model) }
                .startWith(autoMove.autoMoveIfNeeded(viewModel.model))
                .map { it.copy(gameStatus = onMoveCheckFinish.isMatchOver(it)) }
                .map { autoMove.autoMoveIfNeeded(it) }
                .map { it.copy(gameStatus = onMoveCheckFinish.isMatchOver(it)) }
                .subscribe {
                    viewModel.model = it
                    viewModel.updateModel()
                }

        uiEvents
                .ofType(ClickEvent::class.java)
                .takeUntil(stopEvent)
                .filter { it.viewId == R.id.replay }
                .firstElement()
                .subscribe { router.replay(gameType) }
    }

}