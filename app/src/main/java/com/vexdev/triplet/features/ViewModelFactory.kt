package com.vexdev.triplet.features

import android.content.Context
import com.vexdev.triplet.core.ioc.LocatableService
import com.vexdev.triplet.features.game.viewmodels.GameViewModel

/**
 * todo: There should be one of this per feature / domain.
 */
class ViewModelFactory(private val context: Context) : LocatableService {

    fun newGameViewModel() = GameViewModel(context)

}