package com.vexdev.triplet.features

import com.vexdev.triplet.core.ioc.LocatableService
import com.vexdev.triplet.features.game.usecases.AutomatedMove
import com.vexdev.triplet.features.game.usecases.OnGameInputUpdateGameModel
import com.vexdev.triplet.features.game.usecases.OnMoveCheckFinish

/**
 * todo: There should be one of this per feature / domain.
 */
class UseCasesFactory : LocatableService {

    fun newOnGameInputUpdateGameModel() = OnGameInputUpdateGameModel()
    fun newAutomatedMove() = AutomatedMove()
    fun newOnMoveCheckFinish() = OnMoveCheckFinish()

}