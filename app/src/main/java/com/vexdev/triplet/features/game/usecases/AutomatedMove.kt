package com.vexdev.triplet.features.game.usecases

import com.vexdev.triplet.features.game.models.GameModel
import com.vexdev.triplet.features.game.models.GameModel.Companion.CIRCLE
import com.vexdev.triplet.features.game.models.GameModel.Companion.CROSS
import java.util.*

class AutomatedMove {

    /**
     * Performs an automated move if needed.
     * todo: Right now this only supports the Easy mode.
     */
    fun autoMoveIfNeeded(gameModel: GameModel): GameModel {
        if (gameModel.turn > 8) {
            return gameModel
        }
        val crossTurn = gameModel.turn % 2 == 0
        val playerIsCircle = gameModel.playerPiece == CIRCLE
        val opponentTurn = (crossTurn && playerIsCircle) || (!crossTurn && !playerIsCircle)
        if (gameModel.gameType == GameModel.GameType.SINGLE_EASY && opponentTurn) {
            var zeroCount = 0
            gameModel.board.forEach { if (it == 0) zeroCount++ }
            val random = Random()
            var position = random.nextInt(zeroCount)

            val piece = if (gameModel.playerPiece == CIRCLE) CROSS else CIRCLE
            val newBoard = gameModel.board.toMutableList()

            zeroCount = -1
            for (i in 0..8) {
                if (gameModel.board[i] == 0) zeroCount++
                if(zeroCount == position) {
                    position = i
                    break
                }
            }
            newBoard[position] = piece

            return gameModel.copy(board = newBoard)
        }
        return gameModel
    }

}