package com.vexdev.triplet.features.intro.presenters

import android.databinding.ViewDataBinding
import com.vexdev.triplet.core.mvvm.ViewModel
import com.vexdev.triplet.core.mvvm.events.Irrelevant
import com.vexdev.triplet.core.mvvm.events.UiEvent
import com.vexdev.triplet.core.navigation.Presenter
import io.reactivex.Observable

/**
 * Scope reduction! I might keep working on this later...
 */
class WelcomePresenter : Presenter<Any, ViewDataBinding> {
    override fun layout(): Int {
        TODO("not implemented")
    }

    override fun viewModelId(): Int {
        TODO("not implemented")
    }

    override val viewModel: ViewModel<Any>
        get() = TODO("not implemented")
    override val eventGenerator: (ViewDataBinding) -> Observable<UiEvent>
        get() = TODO("not implemented")

    override fun setupSubscriptions(
            uiEvents: Observable<UiEvent>,
            stopEvent: Observable<Irrelevant>) {
        TODO("not implemented")
    }

}