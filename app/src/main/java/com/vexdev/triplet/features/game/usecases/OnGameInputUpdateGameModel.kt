package com.vexdev.triplet.features.game.usecases

import com.vexdev.triplet.R
import com.vexdev.triplet.core.mvvm.events.ClickEvent
import com.vexdev.triplet.features.game.models.GameModel

class OnGameInputUpdateGameModel {

    /**
     * Note: No side effects should exist in those functions.
     */
    fun getUpdatedBoard(
            clickEvent: ClickEvent,
            gameModel: GameModel
    ): GameModel {
        if (gameModel.gameType != GameModel.GameType.MULTI_PLAYER) {
            // Allow a game only on player's turn
            val crossTurn = gameModel.turn % 2 == 0
            val playerIsCross = gameModel.playerPiece == GameModel.CROSS
            if (crossTurn && !playerIsCross) {
                return gameModel
            }
            if (!crossTurn && playerIsCross) {
                return gameModel
            }
        }

        val newBoard = when (clickEvent.viewId) {
            R.id.pos0 -> clickOn(0, gameModel)
            R.id.pos1 -> clickOn(1, gameModel)
            R.id.pos2 -> clickOn(2, gameModel)
            R.id.pos3 -> clickOn(3, gameModel)
            R.id.pos4 -> clickOn(4, gameModel)
            R.id.pos5 -> clickOn(5, gameModel)
            R.id.pos6 -> clickOn(6, gameModel)
            R.id.pos7 -> clickOn(7, gameModel)
            R.id.pos8 -> clickOn(8, gameModel)
            else -> gameModel.board
        }
        return gameModel.copy(board = newBoard)
    }

    private fun clickOn(position: Int, source: GameModel): List<Int> = source
            .board
            .mapIndexed { index, value ->
                if (index == position && value == GameModel.NA) {
                    val crossTurn = source.turn % 2 == 0
                    return@mapIndexed if (source.gameType == GameModel.GameType.MULTI_PLAYER) {
                        if (crossTurn) GameModel.CROSS else GameModel.CIRCLE
                    } else {
                        source.playerPiece
                    }
                }
                return@mapIndexed value
            }
            .toMutableList()

}
