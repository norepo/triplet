package com.vexdev.triplet.features.game.models

/**
 * Models the gaming board into a 9 digits integer where Position zero is the rightmost digit.
 * Follows the position matrix:
 * 8 7 6
 * 5 4 3
 * 2 1 0
 *
 * The digit 1 is a [CROSS] while the digit 2 is a [CIRCLE] and an unset value is represented by 0
 * [NA].
 *
 * Example:
 *
 *                1 2 2      X O O
 * 122111001  ->  1 1 1  ->  X X X
 *                0 0 1          X
 */
data class GameModel(
        val gameType: GameType = GameType.SINGLE_EASY,
        val gameStatus: Int = STATUS_PENDING,
        val board: List<Int> = EMPTY_BOARD,
        val playerPiece: Int = Math.round(Math.random()).toInt() + 1 // Either 1 or 2
) {

    val turn: Int
        get() {
            var turn = 0
            board.forEach { if (it > 0) turn++ }
            return turn
        }

    companion object {
        const val NA: Int = 0
        const val CROSS: Int = 1
        const val CIRCLE: Int = 2
        val EMPTY_BOARD: List<Int> = listOf(
                NA, NA, NA,
                NA, NA, NA,
                NA, NA, NA
        )
        const val STATUS_PENDING = 0
        const val STATUS_WIN_CROSS = CROSS
        const val STATUS_WIN_CIRCLE = CIRCLE
        const val STATUS_DRAW = -1
    }

    enum class GameType {
        MULTI_PLAYER,
        SINGLE_EASY
    }
}