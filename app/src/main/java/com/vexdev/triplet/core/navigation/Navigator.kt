package com.vexdev.triplet.core.navigation

import android.databinding.ViewDataBinding

/**
 * Navigates trough screens in the application. Each screen being configured by a [Presenter].
 * Methods in this interface are called by a [Router] which defines all the paths of the app.
 */
interface Navigator {

    /**
     * Navigates to a presenter.
     * todo This function should accept some configuration to decide the transition animation.
     */
    fun <M, T : ViewDataBinding> navigate(presenter: Presenter<M, T>)

}