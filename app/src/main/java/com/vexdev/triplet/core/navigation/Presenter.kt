package com.vexdev.triplet.core.navigation

import android.databinding.ViewDataBinding
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import com.vexdev.triplet.core.mvvm.ViewModel
import com.vexdev.triplet.core.mvvm.events.Irrelevant
import com.vexdev.triplet.core.mvvm.events.UiEvent
import io.reactivex.Observable

/**
 * Component responsible for presenting a screen.
 */
interface Presenter<M, in DB : ViewDataBinding> {

    /**
     * The layout ID which should be rendered.
     */
    @LayoutRes
    fun layout(): Int

    /**
     * The id of the variable which should contain the ViewModel.
     * By default it's set to zero, meaning there is no ViewModel.
     */
    @IdRes
    fun viewModelId(): Int = 0

    /**
     * The ViewModel for this presenter, if it exists.
     */
    val viewModel: ViewModel<M>?

    val eventGenerator: (DB) -> Observable<UiEvent>

    /**
     * Sets up all reactive subscriptions of this presenter, and provides an observable
     * [stopEvent] that triggers a mandatory release of all resources.
     */
    fun setupSubscriptions(uiEvents: Observable<UiEvent>, stopEvent: Observable<Irrelevant>)

}