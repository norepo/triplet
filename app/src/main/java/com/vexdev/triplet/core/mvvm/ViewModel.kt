package com.vexdev.triplet.core.mvvm

import android.databinding.BaseObservable

abstract class ViewModel<T> protected constructor(var model: T) : BaseObservable() {

    fun updateModel() {
        notifyChange()
    }

}