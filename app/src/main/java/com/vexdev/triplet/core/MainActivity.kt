package com.vexdev.triplet.core

import android.databinding.DataBindingUtil.inflate
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import com.vexdev.triplet.R
import com.vexdev.triplet.core.mvvm.events.*
import com.vexdev.triplet.core.navigation.Navigator
import com.vexdev.triplet.core.navigation.Presenter
import com.vexdev.triplet.core.navigation.Router
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

/**
 * Base activity, responsible for wrapping android events and launching/stopping presenters.
 */
class MainActivity : AppCompatActivity(), Navigator {
    private lateinit var rootView: ViewGroup
    private val lifecycle: BehaviorSubject<LifecycleEvents> = BehaviorSubject.create()
    private val presenterStopEvents: PublishSubject<Irrelevant> = PublishSubject.create()
    private val router: Router = TripletApplication.serviceLocator.get()!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        rootView = findViewById(R.id.content) as ViewGroup
        lifecycle.onNext(OnCreateEvent)
        router.cleanStart(this)
    }

    override fun onStart() {
        super.onStart()
        lifecycle.onNext(OnStartEvent)
    }

    override fun onResume() {
        super.onResume()
        lifecycle.onNext(OnResumeEvent)
    }

    override fun onPause() {
        super.onPause()
        lifecycle.onNext(OnPauseEvent)
    }

    override fun onStop() {
        super.onStop()
        lifecycle.onNext(OnStopEvent)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.onNext(OnDestroyEvent)
        presenterStopEvents.onNext(Irrelevant)
        lifecycle.onComplete()
        presenterStopEvents.onComplete()
    }

    override fun <M, T:ViewDataBinding> navigate(presenter: Presenter<M, T>) {
        // Cleanup first the previous views and presenters, if any
        rootView.removeAllViews()
        presenterStopEvents.onNext(Irrelevant)
        // Setup the new views and presenter.
        val b = inflate<T>(layoutInflater, presenter.layout(), rootView, true)
        if (presenter.viewModelId() != 0) {
            b.setVariable(presenter.viewModelId(), presenter.viewModel)
        }
        presenter.setupSubscriptions(getMergedUiEvents(b, presenter), presenterStopEvents)
    }

    private fun <M, T:ViewDataBinding> getMergedUiEvents(
            dataBinding: T,
            presenter: Presenter<M, T>): Observable<UiEvent> {
        val presenterEvents = presenter.eventGenerator.invoke(dataBinding)
        return Observable.merge(presenterEvents, lifecycle).share()
    }

}
