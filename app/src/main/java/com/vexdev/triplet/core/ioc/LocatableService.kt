package com.vexdev.triplet.core.ioc

/**
 * Defines a service that can be located trough the [ServiceLocator].
 */
interface LocatableService