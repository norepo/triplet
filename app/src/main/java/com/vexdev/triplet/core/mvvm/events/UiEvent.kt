package com.vexdev.triplet.core.mvvm.events

/**
 * Any event that can be generated from the user actions, including the android lifecycle.
 */
interface UiEvent