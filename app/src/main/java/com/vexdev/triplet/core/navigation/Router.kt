package com.vexdev.triplet.core.navigation

import com.vexdev.triplet.core.ioc.LocatableService
import com.vexdev.triplet.features.game.models.GameModel
import com.vexdev.triplet.features.game.presenters.GamePresenter
import com.vexdev.triplet.features.intro.presenters.GameTypePresenter

/**
 * Responsible for the navigation in the app.
 * todo Obviously this concept needs to be expanded. What about back navigation?
 */
class Router: LocatableService {

    lateinit var navigator: Navigator

    fun cleanStart(navigator: Navigator) {
        this.navigator = navigator
        navigator.navigate(GameTypePresenter())
    }

    fun easyGame() {
        navigator.navigate(GamePresenter(GameModel.GameType.SINGLE_EASY))
    }

    fun multiGame() {
        navigator.navigate(GamePresenter(GameModel.GameType.MULTI_PLAYER))
    }

    fun replay(gameType: GameModel.GameType) {
        navigator.navigate(GamePresenter(gameType))
    }

}