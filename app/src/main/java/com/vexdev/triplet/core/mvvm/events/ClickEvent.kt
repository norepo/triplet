package com.vexdev.triplet.core.mvvm.events

import android.support.annotation.IdRes

data class ClickEvent(@IdRes val viewId: Int) : UiEvent