package com.vexdev.triplet.core.mvvm.events

/**
 * Mimics the android lifecycle.
 */
sealed class LifecycleEvents : UiEvent

object OnCreateEvent : LifecycleEvents()
object OnStartEvent : LifecycleEvents()
object OnResumeEvent : LifecycleEvents()
object OnPauseEvent : LifecycleEvents()
object OnStopEvent : LifecycleEvents()
object OnDestroyEvent : LifecycleEvents()