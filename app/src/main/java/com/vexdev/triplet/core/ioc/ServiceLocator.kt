package com.vexdev.triplet.core.ioc

import android.support.annotation.VisibleForTesting

/**
 * Used to store singleton instances of classes that can be collected trough the application.
 */
class ServiceLocator {

    val map: HashMap<Class<in LocatableService>, LocatableService> = hashMapOf()

    /**
     * Adds a service for the first and last time. This method will fail if the service is already
     * registered.
     */
    fun add(service: LocatableService) {
        if (map.containsKey(service.javaClass)) {
            throw IllegalArgumentException("Trying to register an already registered service.")
        }
        put(service)
    }

    /**
     * Overrides an existing service. This method will fail if the service is not registered.
     * This method is meant to be called from tests only.
     */
    @VisibleForTesting
    fun override(service: LocatableService) {
        if (!map.containsKey(service.javaClass)) {
            throw IllegalArgumentException("There is no service to override for " +
                    service.javaClass.canonicalName)
        }
        put(service)
    }

    /**
     * Gets a registered service object.
     */
    @Suppress("UNCHECKED_CAST")
    inline fun <reified T : LocatableService> get(): T? = (map as Map<Class<T>, T>)[T::class.java]

    private fun put(service: LocatableService) = map.put(service.javaClass, service)

}