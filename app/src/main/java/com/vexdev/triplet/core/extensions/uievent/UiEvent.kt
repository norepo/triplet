package com.vexdev.triplet.core.extensions.uievent

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import com.vexdev.triplet.core.mvvm.events.ClickEvent
import io.reactivex.Observable

fun View.clickEvents(): Observable<ClickEvent> = RxView.clicks(this).map { ClickEvent(this.id) }