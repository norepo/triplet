package com.vexdev.triplet.core

import android.app.Application
import com.vexdev.triplet.core.ioc.ServiceLocator
import com.vexdev.triplet.core.navigation.Router
import com.vexdev.triplet.features.UseCasesFactory
import com.vexdev.triplet.features.ViewModelFactory

class TripletApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        registerServices()
    }

    private fun registerServices() {
        serviceLocator.add(Router())
        serviceLocator.add(ViewModelFactory(this))
        serviceLocator.add(UseCasesFactory())
    }

    companion object {
        lateinit var instance: TripletApplication private set
        val serviceLocator: ServiceLocator = ServiceLocator()
    }

}